angular.module('smsApp')
	.run(['$rootScope', 'CONSTANTS',
	        function($rootScope, CONSTANTS){
	            // Restangular.setErrorInterceptor(function(response){
	            //     console.log("Error ",response.status);
	                
	            // });            
	            //keys for constants to be added to $rootScope
	            var CONSTANTS_TO_ADD = ["THEME_CONSTANTS", "currentYear"];

	            // //pick IVI_CONSTANTS_TO_ADD and add to $rootScope
	            _.extend($rootScope, _.pick(CONSTANTS, CONSTANTS_TO_ADD));
	            // $rootScope.templateUrls = CONSTANTS.templateUrls;
	            $rootScope.navigated = false;
	            // $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
	            //     if(previous){
	            //         $rootScope.navigated = true;
	            //     }
	            // });

	            // //Redirection based on permission
	            // $rootScope.$on( "$routeChangeStart", function(event, next, current) {
	            //     var nextController = next.$$route.controller;
	            //     // console.log("nextController",nextController);
	            //     // if(nextController){
	                    
	            //     // }
	            // });
	}]);
