'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
  .module('smsApp', [

    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'restangular',

  ])
  .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','RestangularProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,RestangularProvider) {
    
    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

      // Interceptors Req/begin
    RestangularProvider.setFullRequestInterceptor(function(element, operation, route, url, headers, params) {
       console.log("setFullRequestInterceptor");
        return {
            element: element,
            params: params,
            headers: headers
        };
    });

    RestangularProvider.setResponseInterceptor(function(data, operation, what, url, response) {
       console.log("response setResponseInterceptor");
    });

    RestangularProvider.setBaseUrl("http://hello.com");



    $urlRouterProvider.otherwise('/school/student/');

    $stateProvider
      .state('school', {
        url:'/school',
        templateUrl: 'views/dashboard/main.html',
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'smsApp',
                    files:[
                    'scripts/directives/header/header.js',
                    'scripts/directives/header/header-notification/header-notification.js',
                    'scripts/directives/sidebar/sidebar.js',
                    'scripts/directives/sidebar/sidebar-search/sidebar-search.js',
                    'scripts/directives/validationDirective.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('school.home',{
        url:'/home',
        controller: 'MainCtrl',
        templateUrl:'views/dashboard/home.html',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'smsApp',
              files:[
              'scripts/controllers/main.js',
              'scripts/directives/timeline/timeline.js',
              'scripts/directives/notifications/notifications.js',
              'scripts/directives/chat/chat.js',
              'scripts/directives/dashboard/stats/stats.js'
              ]
            })
          }
        }
      })
      .state('school.form',{
        templateUrl:'views/form.html',
        url:'/form'
    })
      .state('school.blank',{
        templateUrl:'views/pages/blank.html',
        url:'/blank'
    })
      .state('login',{
        templateUrl:'views/pages/login.html',
        url:'/login'
    })
      .state('school.chart',{
        templateUrl:'views/chart.html',
        url:'/chart',
        controller:'ChartCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'bower_components/angular-chart.js/dist/angular-chart.min.js',
                'bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'smsApp',
                files:['scripts/controllers/chartContoller.js']
            })
          }
        }
    })
      .state('school.table',{
        templateUrl:'views/table.html',
        url:'/table'
    })
      .state('school.panels-wells',{
          templateUrl:'views/ui-elements/panels-wells.html',
          url:'/panels-wells'
      })
      .state('school.buttons',{
        templateUrl:'views/ui-elements/buttons.html',
        url:'/buttons'
    })
      .state('school.notifications',{
        templateUrl:'views/ui-elements/notifications.html',
        url:'/notifications'
    })
      .state('school.typography',{
       templateUrl:'views/ui-elements/typography.html',
       url:'/typography'
   })
      .state('school.icons',{
       templateUrl:'views/ui-elements/icons.html',
       url:'/icons'
   })
      .state('school.grid',{
       templateUrl:'views/ui-elements/grid.html',
       url:'/grid'
   }).state('school.student',{
        templateUrl:'views/student.html',
        url:'/student/:studentId',
        controller:'studentCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'smsApp',
                files:['scripts/controllers/studentController.js','scripts/services/studentService.js']
            })
          }
        }
    })
  }])
