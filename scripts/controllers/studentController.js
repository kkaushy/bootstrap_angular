'use strict';

angular.module('smsApp')
  .controller('studentCtrl', function($scope, CONSTANTS,  $stateParams, Restangular) {
  	$scope.CONSTANTS = CONSTANTS;

  //   var resource = Restangular.all('rest/your_db');
  //       resource.get(22).then(function(response){
  //     console.log(response);
  // });

	// $scope.today = function() {	
	    
	// };
 //  	$scope.today();
 $scope.id = $stateParams.studentId;
$scope.dt = new Date();
  // // Disable weekend selection
  // $scope.disabled = function(date, mode) {
  //   return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  // };

  // $scope.toggleMin = function() {
  //   $scope.minDate = $scope.minDate ? null : new Date();
  // };
  // $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: CONSTANTS.DATEFORMAT,
    startingDay: 1
  };

  // $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  // $scope.format = $scope.formats[0];
});